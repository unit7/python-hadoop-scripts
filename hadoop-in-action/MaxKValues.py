#!/usr/bin/env python

import sys

def sort(list):
	return sorted(list, key=lambda pair: pair[0]);

def minInList(list):
	if (len(list) == 0):
		return 0;
	else:
		return list[0][0];

def pushFront(list, val, maxsize):
	if (len(list) < maxsize):
		list.append(val)
	else:
		list[0] = val;

k = int(sys.argv[1])
maxlist = [];

for line in sys.stdin:
	keyvalue = line.split('\t');
	value = int(keyvalue[1]);
	minvalue = minInList(maxlist);
	if (value > minvalue or len(maxlist) < k):
		pushFront(maxlist, (value, keyvalue[0]), k);
		maxlist = sort(maxlist);
else:
	for (key, value) in maxlist:
		print value + '\t' + str(key)
