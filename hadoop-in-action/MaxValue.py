#!/usr/bin/env python

import sys

max = 0;
patent = '';

for line in sys.stdin:
	keyvalue = line.split('\t');
	value = int(keyvalue[1]);
	if (value > max):
		max = value;
		patent = keyvalue[0];
else:
	print patent + '\t' + str(max)
